# OpenML dataset: Tweets-about-distance-learning

https://www.openml.org/d/43847

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
202.645 tweets collected with Twitter API by using the following hashtags and keywords,
distancelearning, onlineschool, onlineteaching, virtuallearning, onlineducation, distanceeducation, OnlineClasses, DigitalLearning, elearning, onlinelearning, distance learning, online teaching, online education, online course, online semester, distance course, distance education, online class, e-learning, e learning
Content
RangeIndex: 202645 entries, 0 to 202644
Data columns (total 8 columns):
    Column         Non-Null Count   Dtype 
---  ------         --------------   ----- 
 0   Unnamed: 0     202645 non-null  int64 
 1   Unnamed: 0.1   202645 non-null  int64 
 2   Content        202645 non-null  object
 3   Location       155123 non-null  object
 4   Username       202645 non-null  object
 5   Retweet-Count  202645 non-null  int64 
 6   Favorites      202645 non-null  int64 
 7   Created at     202645 non-null  object
dtypes: int64(4), object(4)
memory usage: 12.4+ MB
None
Since: 2020-07-23 23:51:34
Until 2020-08-14 05:43:52

Project
I have used this data for sentiment analysis. You can look at my work on GitHub and Medium

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43847) of an [OpenML dataset](https://www.openml.org/d/43847). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43847/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43847/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43847/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

